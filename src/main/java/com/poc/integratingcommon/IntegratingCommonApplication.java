package com.poc.integratingcommon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IntegratingCommonApplication {

    public static void main(String[] args) {
        SpringApplication.run(IntegratingCommonApplication.class, args);
    }

}
