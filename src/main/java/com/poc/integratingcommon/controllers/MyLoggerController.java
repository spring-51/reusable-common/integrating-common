package com.poc.integratingcommon.controllers;

import com.poc.common.service.LoggerService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/logs")
public class MyLoggerController {

    private final LoggerService loggerService;

    public MyLoggerController(LoggerService loggerService) {
        this.loggerService = loggerService;
    }

    @GetMapping(value = "/info")
    public void info(@RequestParam String msg){
        loggerService.info(msg);
    }
}
