package com.poc.integratingcommon.controllers;

import com.poc.common.constants.MyConstants;
import com.poc.common.service.MyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MyController {

    private final MyService myService;

    public MyController(MyService myService) {
        this.myService = myService;
    }

    @GetMapping
    public String getStringFromMyConstants(){
        return MyConstants.PROJECT_NAME;
    }

    @GetMapping("/service")
    public String getStringFromService(){
        return myService.returnString();
    }
}
