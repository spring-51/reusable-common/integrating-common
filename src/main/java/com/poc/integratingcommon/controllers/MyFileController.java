package com.poc.integratingcommon.controllers;

import com.poc.common.service.MyFileService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequestMapping(value = "/file")
public class MyFileController {
    private final MyFileService myFileService;

    public MyFileController(MyFileService myFileService) {
        this.myFileService = myFileService;
    }

    @GetMapping(value = "/create")
    public String createFile(@RequestParam String fileName) throws IOException {
        String filePath = String.format("./test/files/MyFileController/%s", fileName);
        Boolean fileCreated = myFileService.create(filePath);
        if(fileCreated){
            return String.format("File successfully created at - '%s'", filePath);
        }else{
            return String.format("File creation failed for - '%s'", fileName);
        }
    }

    @GetMapping(value = "/exists")
    public String exists(@RequestParam String fileName) {
        String filePath = String.format("./test/files/MyFileController/%s", fileName);
        Boolean exists = myFileService.exists(filePath);
        if(exists){
            return String.format("File exists fileName - '%s'", fileName);
        }else{
            return String.format("File does not exist fileName - '%s'", fileName);
        }
    }


}
