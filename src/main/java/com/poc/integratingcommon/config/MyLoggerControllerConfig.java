package com.poc.integratingcommon.config;

import com.poc.common.service.LoggerService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MyLoggerControllerConfig {

    @Bean
    public LoggerService loggerService(){
        return new LoggerService();
    }
}
