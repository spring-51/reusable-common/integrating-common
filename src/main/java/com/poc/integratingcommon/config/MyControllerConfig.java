package com.poc.integratingcommon.config;

import com.poc.common.repo.MyRepo;
import com.poc.common.service.MyService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MyControllerConfig {

    @Bean
    public MyService myService(){
        return new MyService(getMyRepo());
    }

    @Bean
    public MyRepo getMyRepo() {
        return new MyRepo();
    }
}
