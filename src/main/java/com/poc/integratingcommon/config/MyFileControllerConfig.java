package com.poc.integratingcommon.config;

import com.poc.common.repo.MyFileRepo;
import com.poc.common.service.MyFileService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MyFileControllerConfig {
    @Bean
    public MyFileService myFileService(){
        return new MyFileService(getMyFileRepo());
    }

    @Bean
    public MyFileRepo getMyFileRepo() {
        return new MyFileRepo();
    }
}
