package com.poc.integratingcommon.controllers;

import com.poc.common.repo.MyRepo;
import com.poc.common.service.MyService;
import org.junit.jupiter.api.Test;
import org.springframework.util.ObjectUtils;

import static org.junit.jupiter.api.Assertions.*;

class MyControllerTest {

    @Test
    public void test(){
        MyController controller = new MyController(new MyService(new MyRepo()));
        System.out.println(controller.getStringFromMyConstants());
        assertTrue(!ObjectUtils.isEmpty(controller.getStringFromMyConstants()));
    }
}